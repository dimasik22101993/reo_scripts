type AsyncForEachCb<T> = (item: T, idx: number) => Promise<void>;


export class ExtendedArray<T> extends Array<T> {

  constructor(...args: T[]) {
    super(...args);
  }

  async asyncForEach(cb: AsyncForEachCb<T>): Promise<void> {
    for (const [idx, item] of this.entries()) {
      await cb(item, idx);
    }
  }
}
