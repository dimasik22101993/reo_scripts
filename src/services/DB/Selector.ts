import { Primitive } from "./types/Primitive.js";
import { DB } from "./DB.js";
import { ExtendedArray } from "../ExtendedArray/ExtendedArray.js";


interface Nested {
  name: string, 
  selector: Selector,
}

export interface SelectorResult {
  [k: string]: any
}

const IN_REGEX = /#in\((\w+(\.\w+)?)?(?:,\s*(\w+))?\)/;


export class SelectorBuilder {
  private sql?: string;
  private parameters: Primitive[] = [];
  private nesteds: Nested[] = [];
  private parent?: SelectorBuilder;

  setSql(sql: string): SelectorBuilder {
    this.sql = sql;
    return this;
  }

  getSql(): string | undefined {
    return this.sql;
  }

  getNesteds() {
    return this.nesteds;
  }

  getParameters() {
    return this.parameters;
  }

  getParent(): SelectorBuilder | undefined {
    return this.parent;
  }

  setParent(parent: SelectorBuilder) {
    this.parent = parent;
  }

  addParameter(parameter: Primitive): SelectorBuilder {
    this.parameters.push(parameter);
    return this;
  }

  addParameters(parameters: Primitive[]): SelectorBuilder {
    this.parameters.push(...parameters);
    return this;
  }

  addNested(nested: Nested): SelectorBuilder {
    this.nesteds.push(nested);
    return this;
  }

  build(): Selector {
    return new Selector(this);
  }
}


export class Selector {
  private parentResult?: SelectorResult[];

  constructor(
    private builder: SelectorBuilder, 
  ) {}

  setParentResults(parentResult: SelectorResult[]) {
    this.parentResult = parentResult;
  }

  async getMany(): Promise<SelectorResult[]> {
    let sql = this.builder.getSql();
    if (!sql) throw new Error('SQL query is required');
    
    const list: SelectorResult[] = await DB.select(this.builder.getSql()!, this.builder.getParameters());
    const nesteds = new ExtendedArray(...this.builder.getNesteds());
    if (nesteds.length) {
      await nesteds.asyncForEach(async nested => {
        let listValues: any[] = [];
        let firstInArgument: string | undefined = undefined;
        let secondInArgument: string | undefined = undefined;

        let nestedSql = nested.selector.builder.getSql();
        const inMatches = nestedSql!.match(IN_REGEX);
        if (inMatches) {
          if (!list.length) return;
          
          const inPart = inMatches[0];
          const firstArgument = inMatches[1];
          const firstArgumentParts = firstArgument.split('.').map(x => x.trim());
          firstInArgument = firstArgumentParts.length === 2 
            ? firstArgumentParts[1] 
            : firstArgumentParts[0];

          secondInArgument = inMatches[3] || 'id';

          listValues = list
            .map(x => x[secondInArgument!])
            .filter(x => x !== undefined && x !== null);

          let result: string | undefined = listValues.length ? undefined : '0';

          if (!result) {
            const listValueType = typeof(listValues[0]);
            if (listValueType === 'boolean') {
              result = ` ${firstArgument} IN (${listValues.map(x => x ? 1 : 0).join(',')})`;
            } else if (listValueType === 'string') {
              result = ` ${firstArgument} IN (${listValues.map(x => "'" + x + "'").join(',')})`;
            } else {
              result = ` ${firstArgument} IN (${listValues.join(',')})`;
            }
          }
          
          nestedSql = nestedSql!.replace(inPart, result);
        }

        nested.selector.builder.setSql(nestedSql!);
        const nestedResult = await nested.selector.getMany();
        list.forEach(x => {
          x[nested.name] = nestedResult.filter(y => y[firstInArgument!] === x[secondInArgument!]);
        });
      });
    }

    return list;
  }

  async getOne(): Promise<SelectorResult | undefined> {
    const list = await this.getMany();
    return list[0];
  }

  static Builder = SelectorBuilder;
}
