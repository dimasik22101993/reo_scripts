import * as mysql from 'mysql2/promise';
import { Primitive } from './types/Primitive.js';


export class DB {

  static async sendQuery(sql: string, values: Primitive[] = []) {
    const con = await mysql.createConnection({
      host: 'localhost',
      user: 'root',
      database: 'reo_script',
    });

    await con.connect();
    let result: any;
    
    try {
      console.log(sql, values)
      result = await con.query(sql, values);
    } catch (err) {
      console.error(err);
      console.log('-----');
      console.log(sql, ' | ', values);
      console.log('-----');
    }
    await con.end();
    return result;
  }

  static async select(sql: string, values: Primitive[] = []): Promise<{[k: string]: Primitive}[]> {
    const result = await this.sendQuery(sql, values);
    return result[0] as any;
  }

  static async insert(tableName: string, data: {[k: string]: Primitive}) {
    const keys = Object.keys(data);
    const sql = `INSERT INTO \`${tableName}\` (${keys.map(key => '`' + key + '`').join(',')}) VALUES (${keys.map(_ => '?').join(',')})`;
    return this.sendQuery(sql, keys.map(key => data[key]));
  }

  static async insertMany(tableName: string, list: {[k: string]: Primitive}[]) {
    const values: Primitive[] = [];
    const keys = new Set<string>();

    list.forEach(data => {
      const ks = Object.keys(data);
      ks.forEach(k => keys.add(k));
    });

    const keysArr = Array.from(keys);
    const parts = list.map(_ => `(${keysArr.map(_ => '?').join(',')})`);
    list.forEach(x => {
      keysArr.forEach(key => values.push(x[key]));
    });

    const sql = `INSERT INTO \`${tableName}\` (${keysArr.map(key => '`' + key + '`').join(',')}) VALUES ${parts.join(',')}`;
    return this.sendQuery(sql, values);
  }
}
