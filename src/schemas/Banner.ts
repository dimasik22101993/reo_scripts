import * as mongoose from 'mongoose';


const BannerSchema = new mongoose.Schema({
  ad_file: Number,
  advertisement: Number,
  created: Number,
  domain: Number,
  ip: String,
  language: String,
  languages: String,
  origin: String,
  pathname: String,
}, {
  collection: 'banner',
});

export const Banner = mongoose.model('Banner', BannerSchema);