import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const BannerlessSectionSchema = new Schema({
  created: Number,
  width: Number,
  height: Number,
  index: Number,
  ip: String,
  origin: String,
  pathname: String,
  domain: Number,
}, {
  collection: 'bannerless_section',
});

export const BannerlessSection = mongoose.model('BannerlessSection', BannerlessSectionSchema);
