import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const BannerAddedSchema = new Schema({
  ad_file: Number,
  advertisement: Number,
  created: Number,
  domain: Number,
  ip: String,
  origin: String,
  pathname: String,
}, {
  collection: 'banner_added',
});

export const BannerAdded = mongoose.model('BannerAdded', BannerAddedSchema);
