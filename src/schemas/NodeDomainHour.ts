import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


export const NodeDomainHourSchema = new Schema({
  banner: Number,
  banner_added: Number,
  banner_loaded: Number,
  banner_shown: Number,
  banner_clicked: Number,
  bannerless_section: Number,
  script_loaded: Number,
  script_downloaded: Number,
  domain: Number,
  start: Number,
  end: Number,
}, {
  collection: 'node_domain_hour',
});

export const NodeDomainHour = mongoose.model('NodeDomainHour', NodeDomainHourSchema);
