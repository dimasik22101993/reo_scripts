import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


export const DomainMonthSchema = new Schema({
  banner: Number,
  banner_added: Number,
  banner_clicked: Number,
  banner_loaded: Number,
  banner_shown: Number,
  bannerless_section: Number,
  script_loaded: Number,
  script_downloaded: Number,
  domain: Number,
  start: Number,
  end: Number,
}, {
  collection: 'domain_month',
});

export const DomainMonth = mongoose.model('DomainMonth', DomainMonthSchema);
