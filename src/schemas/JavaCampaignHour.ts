
import * as mongoose from 'mongoose';


const JavaCampaignHourSchema = new mongoose.Schema({
  campaign: Number,
  banner: Number,
  banner_added: Number,
  banner_clicked: Number,
  banner_loaded: Number,
  banner_shown: Number,
  start: Number,
  end: Number,
}, {
  collection: 'java_campaign_hour',
});

export const JavaCampaignHour = mongoose.model('JavaCampaignHour', JavaCampaignHourSchema);