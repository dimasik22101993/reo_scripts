import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const AdFileDomainDaySchema = new Schema({
  ad_file: Number,
  domain: Number,
  banner: Number,
  banner_added: Number,
  banner_clicked: Number,
  banner_loaded: Number,
  banner_shown: Number,
  bannerless_section: Number,
  start: Number,
  end: Number,
  advertisement: Number,
  campaign: Number,
  ad_dimension: Number,
}, {
  collection: 'node_ad_file_domain_day',
});

export const AdFileDomainDay = mongoose.model('AdFileDomainDay', AdFileDomainDaySchema);