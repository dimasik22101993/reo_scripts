import * as mongoose from 'mongoose';


const CampaignHourSchema = new mongoose.Schema({
  campaign: Number,
  banner: Number,
  banner_added: Number,
  banner_clicked: Number,
  banner_loaded: Number,
  banner_shown: Number,
  start: Number,
  end: Number,
}, {
  collection: 'campaign_hour',
});

export const CampaignHour = mongoose.model('CampaignHour', CampaignHourSchema);