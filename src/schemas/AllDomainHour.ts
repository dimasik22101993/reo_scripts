import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


export const AllDomainHourSchema = new Schema({
  banner: Number,
  banner_added: Number,
  banner_clicked: Number,
  banner_loaded: Number,
  banner_shown: Number,
  bannerless_section: Number,
  script_loaded: Number,
  script_downloaded: Number,
  start: Number,
  end: Number,
}, {
  collection: 'all_domain_hour',
});

export const AllDomainHour = mongoose.model('AllDomainHour', AllDomainHourSchema);
