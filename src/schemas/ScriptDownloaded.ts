import * as mongoose from 'mongoose';


const ScriptDownloadedSchema = new mongoose.Schema({
  created: Number,
  domain: Number,
  ip: String,
  language: String,
  languages: String,
  origin: String,
  pathname: String,
}, {
  collection: 'script_downloaded',
});

export const ScriptDownloaded = mongoose.model('ScriptDownloaded', ScriptDownloadedSchema);
