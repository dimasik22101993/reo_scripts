import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const BannerClickedSchema = new Schema({
  ad_file: Number,
  advertisement: Number,
  banner: Number,
  created: Number,
  domain: Number,
  ip: String,
  origin: String,
  pathname: String,
}, {
  collection: 'banner_clicked',
});

export const BannerClicked = mongoose.model('BannerClicked', BannerClickedSchema);
