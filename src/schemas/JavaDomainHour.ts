import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const JavaDomainHourSchema = new Schema({
  banner: Number,
  banner_added: Number,
  banner_loaded: Number,
  banner_shown: Number,
  banner_clicked: Number,
  bannerless_section: Number,
  script_loaded: Number,
  script_downloaded: Number,
  domain: Number,
  start: Number,
  end: Number,
}, {
  collection: 'java_domain_hour',
});

export const JavaDomainHour = mongoose.model('JavaDomainHour', JavaDomainHourSchema);
