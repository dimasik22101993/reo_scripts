import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const DomainCampaignDaySchema = new Schema({
  domain: Number,
  campaign: Number,
  banner: Number,
  banner_added: Number,
  banner_clicked: Number,
  banner_loaded: Number,
  banner_shown: Number,
  bannerless_section: Number,
  start: Number,
  end: Number,
}, {
  collection: 'node_domain_campaign_day',
});

export const DomainCampaignDay = mongoose.model('DomainCampaignDay', DomainCampaignDaySchema);
