import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const JavaDomainDaySchema = new Schema({
  banner: Number,
  banner_added: Number,
  banner_clicked: Number,
  banner_loaded: Number,
  banner_shown: Number,
  bannerless_section: Number,
  script_loaded: Number,
  script_downloaded: Number,
  domain: Number,
  start: Number,
  end: Number,
}, {
  collection: 'java_domain_day',
});

export const JavaDomainDay = mongoose.model('JavaDomainDay', JavaDomainDaySchema);
