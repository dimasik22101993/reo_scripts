import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';


const BannerShownSchema = new Schema({
  ad_file: Number,
  advertisement: Number,
  created: Number,
  domain: Number,
  ip: String,
  origin: String,
  pathname: String,
}, {
  collection: 'banner_shown',
});

export const BannerShown = mongoose.model('BannerShown', BannerShownSchema);
