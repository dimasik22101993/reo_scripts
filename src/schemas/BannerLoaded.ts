import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';

const BannerLoadedSchema = new Schema({
  ad_file: Number,
  advertisement: Number,
  created: Number,
  domain: Number,
  ip: String,
  origin: String,
  pathname: String,
}, {
  collection: 'banner_loaded',
});

export const BannerLoaded = mongoose.model('BannerLoaded', BannerLoadedSchema);
