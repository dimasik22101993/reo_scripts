import * as mongoose from 'mongoose';


const ScriptLoadedSchema = new mongoose.Schema({
  created: Number,
  domain: Number,
  ip: String,
  language: String,
  languages: String,
  origin: String,
  pathname: String,
}, {
  collection: 'script_loaded',
});

export const ScriptLoaded = mongoose.model('ScriptLoaded', ScriptLoadedSchema);
