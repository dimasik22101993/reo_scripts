import express, { Application } from "express";
import * as dotenv from 'dotenv';
import App from "./app/app.js";
import * as mongoose from 'mongoose';

dotenv.config();


const {
  PORT, 
  MONGO_USER, 
  MONGO_PASSWORD, 
  MONGO_IP, 
  MONGO_PORT, 
  MONGO_DB_NAME
} = process.env as any;


const app: Application = express();
listen();

function listen() {
  app.listen(+PORT, async () => {
    console.log(`Server running on port ${PORT}`);
    mongoose.set('strictQuery', true);
    await mongoose.connect(`mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_IP}:${MONGO_PORT}/${MONGO_DB_NAME}`);
    console.log('===============');
    console.log('CONNECTED TO DATABASE');
    console.log('===============');
    
    new App();
  });
}
