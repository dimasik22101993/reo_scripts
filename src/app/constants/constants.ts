export const MS_IN_SECOND = 1000;
export const MS_IN_MINUTE = MS_IN_SECOND * 60;
export const MS_IN_HOUR = MS_IN_MINUTE * 60;
export const MS_IN_DAY = MS_IN_HOUR * 24;

export const ADS = [
  { id: "8", ad_name: "WOLW1", campaign: "9", campaign_name: "WOLW1" },
  {
    id: "9",
    ad_name: "REKLAMA TESTOWA",
    campaign: "11",
    campaign_name: "KAMPANIA TESTOWA",
  },
  { id: "10", ad_name: "TLOVE", campaign: "12", campaign_name: "T.LOVE" },
  {
    id: "11",
    ad_name: "PETERSON 1",
    campaign: "14",
    campaign_name: "KAMPANIA PETERSON 02.2024",
  },
  {
    id: "12",
    ad_name: "PETERSON 1 (v2)",
    campaign: "16",
    campaign_name: "KAMPANIA PETERSON 2 03.2024",
  },
  {
    id: "13",
    ad_name: "Media Expert 1",
    campaign: "15",
    campaign_name: "Media Expert kampania 1 02.2024",
  },
  {
    id: "14",
    ad_name: "PETERSON 2",
    campaign: "16",
    campaign_name: "KAMPANIA PETERSON 2 03.2024",
  },
  {
    id: "15",
    ad_name: "test top layer",
    campaign: "17",
    campaign_name: "TOP LAYER TEST",
  },
  {
    id: "16",
    ad_name: "Olej Kujawski 1 04.2024 ",
    campaign: "18",
    campaign_name: "Olej Kujawski 1 04.2024",
  },
  {
    id: "17",
    ad_name: "Olej Kujawski 1 04.2024 TOP LAYER TYP 1 Siec portali",
    campaign: "19",
    campaign_name: "Olej Kujawski 1 04.2024 TOP LAYER TYP 1 Siec portali",
  },
  {
    id: "18",
    ad_name: "Olej Kujawski 1 04.2024 TOP LAYER TYP 2 KLASTER MEDIOW LOKALNYCH",
    campaign: "20",
    campaign_name:
      "Olej Kujawski 1 04.2024 TOP LAYER TYP 2 Klaster Mediow Lokalnych",
  },
  { id: "19", ad_name: "DEV", campaign: "13", campaign_name: "DEVELOPERSKA" },
  {
    id: "20",
    ad_name: "ELORI 1 4.05.2024",
    campaign: "21",
    campaign_name: "ELORI Kampania 1 w REO 14.05.2024",
  },
].map(ad => {
  return {
    ...ad,
    id: +ad.id,
    campaign: +ad.campaign,
  }
});

export const AD_FILES = [
  {
      "id": 19,
      "advertisement": 8,
      "ad_dimension": 6,
      "campaign": 9
  },
  {
      "id": 20,
      "advertisement": 8,
      "ad_dimension": 1,
      "campaign": 9
  },
  {
      "id": 21,
      "advertisement": 8,
      "ad_dimension": 4,
      "campaign": 9
  },
  {
      "id": 22,
      "advertisement": 9,
      "ad_dimension": 6,
      "campaign": 11
  },
  {
      "id": 23,
      "advertisement": 9,
      "ad_dimension": 1,
      "campaign": 11
  },
  {
      "id": 24,
      "advertisement": 9,
      "ad_dimension": 4,
      "campaign": 11
  },
  {
      "id": 25,
      "advertisement": 10,
      "ad_dimension": 4,
      "campaign": 12
  },
  {
      "id": 26,
      "advertisement": 0,
      "ad_dimension": 3,
      "campaign": 0
  },
  {
      "id": 27,
      "advertisement": 10,
      "ad_dimension": 3,
      "campaign": 12
  },
  {
      "id": 28,
      "advertisement": 10,
      "ad_dimension": 2,
      "campaign": 12
  },
  {
      "id": 29,
      "advertisement": 10,
      "ad_dimension": 6,
      "campaign": 12
  },
  {
      "id": 30,
      "advertisement": 10,
      "ad_dimension": 5,
      "campaign": 12
  },
  {
      "id": 31,
      "advertisement": 11,
      "ad_dimension": 6,
      "campaign": 14
  },
  {
      "id": 32,
      "advertisement": 11,
      "ad_dimension": 6,
      "campaign": 14
  },
  {
      "id": 33,
      "advertisement": 11,
      "ad_dimension": 6,
      "campaign": 14
  },
  {
      "id": 34,
      "advertisement": 11,
      "ad_dimension": 6,
      "campaign": 14
  },
  {
      "id": 36,
      "advertisement": 11,
      "ad_dimension": 5,
      "campaign": 14
  },
  {
      "id": 37,
      "advertisement": 11,
      "ad_dimension": 4,
      "campaign": 14
  },
  {
      "id": 38,
      "advertisement": 11,
      "ad_dimension": 3,
      "campaign": 14
  },
  {
      "id": 39,
      "advertisement": 11,
      "ad_dimension": 2,
      "campaign": 14
  },
  {
      "id": 40,
      "advertisement": 11,
      "ad_dimension": 1,
      "campaign": 14
  },
  {
      "id": 41,
      "advertisement": 11,
      "ad_dimension": 5,
      "campaign": 14
  },
  {
      "id": 42,
      "advertisement": 11,
      "ad_dimension": 4,
      "campaign": 14
  },
  {
      "id": 43,
      "advertisement": 11,
      "ad_dimension": 3,
      "campaign": 14
  },
  {
      "id": 44,
      "advertisement": 11,
      "ad_dimension": 2,
      "campaign": 14
  },
  {
      "id": 45,
      "advertisement": 11,
      "ad_dimension": 1,
      "campaign": 14
  },
  {
      "id": 46,
      "advertisement": 11,
      "ad_dimension": 5,
      "campaign": 14
  },
  {
      "id": 47,
      "advertisement": 11,
      "ad_dimension": 4,
      "campaign": 14
  },
  {
      "id": 48,
      "advertisement": 11,
      "ad_dimension": 3,
      "campaign": 14
  },
  {
      "id": 49,
      "advertisement": 11,
      "ad_dimension": 2,
      "campaign": 14
  },
  {
      "id": 50,
      "advertisement": 11,
      "ad_dimension": 1,
      "campaign": 14
  },
  {
      "id": 51,
      "advertisement": 0,
      "ad_dimension": 6,
      "campaign": 0
  },
  {
      "id": 52,
      "advertisement": 12,
      "ad_dimension": 6,
      "campaign": 16
  },
  {
      "id": 53,
      "advertisement": 12,
      "ad_dimension": 5,
      "campaign": 16
  },
  {
      "id": 54,
      "advertisement": 12,
      "ad_dimension": 2,
      "campaign": 16
  },
  {
      "id": 55,
      "advertisement": 12,
      "ad_dimension": 3,
      "campaign": 16
  },
  {
      "id": 56,
      "advertisement": 12,
      "ad_dimension": 4,
      "campaign": 16
  },
  {
      "id": 57,
      "advertisement": 0,
      "ad_dimension": 5,
      "campaign": 0
  },
  {
      "id": 58,
      "advertisement": 0,
      "ad_dimension": 4,
      "campaign": 0
  },
  {
      "id": 59,
      "advertisement": 0,
      "ad_dimension": 3,
      "campaign": 0
  },
  {
      "id": 60,
      "advertisement": 0,
      "ad_dimension": 2,
      "campaign": 0
  },
  {
      "id": 61,
      "advertisement": 0,
      "ad_dimension": 1,
      "campaign": 0
  },
  {
      "id": 62,
      "advertisement": 0,
      "ad_dimension": 6,
      "campaign": 0
  },
  {
      "id": 63,
      "advertisement": 13,
      "ad_dimension": 5,
      "campaign": 15
  },
  {
      "id": 64,
      "advertisement": 13,
      "ad_dimension": 4,
      "campaign": 15
  },
  {
      "id": 65,
      "advertisement": 13,
      "ad_dimension": 3,
      "campaign": 15
  },
  {
      "id": 66,
      "advertisement": 13,
      "ad_dimension": 2,
      "campaign": 15
  },
  {
      "id": 67,
      "advertisement": 13,
      "ad_dimension": 1,
      "campaign": 15
  },
  {
      "id": 68,
      "advertisement": 13,
      "ad_dimension": 6,
      "campaign": 15
  },
  {
      "id": 69,
      "advertisement": 14,
      "ad_dimension": 5,
      "campaign": 16
  },
  {
      "id": 70,
      "advertisement": 14,
      "ad_dimension": 4,
      "campaign": 16
  },
  {
      "id": 71,
      "advertisement": 14,
      "ad_dimension": 3,
      "campaign": 16
  },
  {
      "id": 72,
      "advertisement": 14,
      "ad_dimension": 2,
      "campaign": 16
  },
  {
      "id": 73,
      "advertisement": 14,
      "ad_dimension": 6,
      "campaign": 16
  },
  {
      "id": 75,
      "advertisement": 15,
      "ad_dimension": 7,
      "campaign": 17
  },
  {
      "id": 76,
      "advertisement": 0,
      "ad_dimension": 7,
      "campaign": 0
  },
  {
      "id": 84,
      "advertisement": 16,
      "ad_dimension": 3,
      "campaign": 18
  },
  {
      "id": 85,
      "advertisement": 16,
      "ad_dimension": 3,
      "campaign": 18
  },
  {
      "id": 86,
      "advertisement": 16,
      "ad_dimension": 8,
      "campaign": 18
  },
  {
      "id": 87,
      "advertisement": 16,
      "ad_dimension": 2,
      "campaign": 18
  },
  {
      "id": 88,
      "advertisement": 16,
      "ad_dimension": 2,
      "campaign": 18
  },
  {
      "id": 89,
      "advertisement": 17,
      "ad_dimension": 7,
      "campaign": 19
  },
  {
      "id": 90,
      "advertisement": 17,
      "ad_dimension": 7,
      "campaign": 19
  },
  {
      "id": 91,
      "advertisement": 18,
      "ad_dimension": 7,
      "campaign": 20
  },
  {
      "id": 92,
      "advertisement": 18,
      "ad_dimension": 7,
      "campaign": 20
  },
  {
      "id": 93,
      "advertisement": 16,
      "ad_dimension": 6,
      "campaign": 18
  },
  {
      "id": 94,
      "advertisement": 16,
      "ad_dimension": 13,
      "campaign": 18
  },
  {
      "id": 95,
      "advertisement": 19,
      "ad_dimension": 3,
      "campaign": 13
  },
  {
      "id": 96,
      "advertisement": 20,
      "ad_dimension": 3,
      "campaign": 21
  },
  {
      "id": 97,
      "advertisement": 20,
      "ad_dimension": 3,
      "campaign": 21
  },
  {
      "id": 98,
      "advertisement": 20,
      "ad_dimension": 3,
      "campaign": 21
  },
  {
      "id": 99,
      "advertisement": 20,
      "ad_dimension": 1,
      "campaign": 21
  },
  {
      "id": 100,
      "advertisement": 20,
      "ad_dimension": 1,
      "campaign": 21
  },
  {
      "id": 101,
      "advertisement": 20,
      "ad_dimension": 6,
      "campaign": 21
  },
  {
      "id": 102,
      "advertisement": 20,
      "ad_dimension": 6,
      "campaign": 21
  },
  {
      "id": 103,
      "advertisement": 20,
      "ad_dimension": 2,
      "campaign": 21
  },
  {
      "id": 104,
      "advertisement": 20,
      "ad_dimension": 2,
      "campaign": 21
  },
  {
      "id": 105,
      "advertisement": 0,
      "ad_dimension": 2,
      "campaign": 0
  },
  {
      "id": 106,
      "advertisement": 20,
      "ad_dimension": 2,
      "campaign": 21
  }
];
