import { AdFileDomainDay } from "../schemas/AdFileDomainDay.js";
import { Banner } from "../schemas/Banner.js";
import { BannerAdded } from "../schemas/BannerAdded.js";
import { BannerClicked } from "../schemas/BannerClicked.js";
import { BannerLoaded } from "../schemas/BannerLoaded.js";
import { BannerShown } from "../schemas/BannerShown.js";
import { BannerlessSection } from "../schemas/BannerlessSection.js";
import { ADS, AD_FILES, MS_IN_DAY } from "./constants/constants.js";


export class ComputeAdFileDomainDay {
  campaigns = new Map<number, number[]>(); // klucz to id kampanii, wartosć tablica reklam kampanii 
  adCampaign = new Map<number, number>(); // klucz to id reklamy, wartość to id kampanii 
  adFileAd = new Map<number, number>(); // klucz to id baneru, wartość to id reklamy 
  adFileDimension = new Map<number, number>(); // klucz to id baneru, wartość to id rozmiaru 

  constructor() {
    this.init();
  }

  async init() {
    this.defineCampaigns();
    this.defineAdCampaigns();
    this.defineAdFileAd();
    this.defineAdFileDimension();
    this.compute();
  }

  private defineCampaigns() {
    ADS.forEach(ad => {
      const { id, campaign: campaignId } = ad;
      if (!this.campaigns.get(campaignId)) this.campaigns.set(campaignId, []);
      this.campaigns.get(campaignId)!.push(id);
    });
  }

  private defineAdCampaigns() {
    ADS.forEach(ad => {
      this.adCampaign.set(ad.id, ad.campaign);
    });
  }

  private defineAdFileAd() {
    AD_FILES.forEach(adFile => {
      this.adFileAd.set(adFile.id, adFile.advertisement);
    });
  }

  private defineAdFileDimension() {
    AD_FILES.forEach(adFile => {
      this.adFileDimension.set(adFile.id, adFile.ad_dimension);
    });
    console.log(this.adFileDimension)
  }

  private async compute() {
    let n = 1717279200000; //new Date(2024, 3, 1).getTime();
    const nowDate = new Date();
    const end = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0).getTime();

    while (n < end) {
      const domainAdFileDays: {[k: string]: any} = {};
      console.log('---');
      console.log(n);
      const start = Date.now();
      const nextDay = n + MS_IN_DAY;
      const config = {
        created: {
          $gte: n, $lt: nextDay
        }
      }

      await this.computeBanners({
        config,
        domainAdFileDays,
        start: n,
        end: nextDay,
        model: Banner,
        modelName: 'banner',
      });

      await this.computeBanners({
        config,
        domainAdFileDays,
        start: n,
        end: nextDay,
        model: BannerAdded,
        modelName: 'banner_added',
      });

      await this.computeBanners({
        config,
        domainAdFileDays,
        start: n,
        end: nextDay,
        model: BannerClicked,
        modelName: 'banner_clicked',
      });

      await this.computeBanners({
        config,
        domainAdFileDays,
        start: n,
        end: nextDay,
        model: BannerLoaded,
        modelName: 'banner_loaded',
      });

      await this.computeBanners({
        config,
        domainAdFileDays,
        start: n,
        end: nextDay,
        model: BannerShown,
        modelName: 'banner_shown',
      });

      await this.computeBanners({
        config,
        domainAdFileDays,
        start: n,
        end: nextDay,
        model: BannerlessSection,
        modelName: 'bannerless_section',
      });

      const list = Object.values(domainAdFileDays);
      console.log('list')
      console.log(list.length)
      for (let x of list) {
        await x.save();
      }

      n = nextDay;
      console.log((Date.now() - start) / 1000, 's');
    }
  }

  private async computeBanners(data: {
    config: any,
    domainAdFileDays: any,
    start: number,
    end: number,
    model: any,
    modelName: string,
  }) {
    let skip = 0;
    const limit = 3_000_000;
    
    while (true) {
      const banners: any[] = await data.model
        .find(data.config)
        .skip(skip)
        .limit(limit)
        .exec();
      console.log('skip', skip);
      console.log(data.modelName, banners.length);
      banners.forEach((banner: any) => {
        const adFileId = banner.ad_file;
        if (!adFileId) return;
        
        const adId = banner.advertisement || 0;

        const domainId = banner.domain;
        const campaignId = this.adCampaign.get(adId) || 0;
        const adDimension = this.adFileDimension.get(adFileId) || 0;
        if (!domainId) return;

        const key = domainId + '_' + adFileId;
        this.setAdFileDomainDays(data.domainAdFileDays, {
          campaign: campaignId,
          domain: domainId,
          ad_dimension: adDimension,
          ad_file: adFileId,
          advertisement: adId,
          start: data.start,
          end: data.end,
        });

        ++data.domainAdFileDays[key][data.modelName];
      });

      if (banners.length >= limit) {
        skip += limit;
        continue;
      }

      break;
    }
  }

  private setAdFileDomainDays(domainCampaignDays: {[k: string]: any}, data: {
    campaign: number,
    domain: number,
    ad_dimension: number,
    ad_file: number,
    advertisement: number,
    start: number,
    end: number,
  }) {
    const domainId = data.domain;
    const adFileId = data.ad_file;
    const key = domainId + '_' + adFileId;

    if (!domainCampaignDays[key]) {
      domainCampaignDays[key] = new AdFileDomainDay({
        ...data,
        banner: 0,
        banner_added: 0,
        banner_clicked: 0,
        banner_loaded: 0,
        banner_shown: 0,
        bannerless_section: 0,
      });
    }
  }
}
