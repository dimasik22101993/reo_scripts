import { Banner } from "../schemas/Banner.js";
import { DomainDay } from "../schemas/DomainDay.js";
import { JavaDomainDay } from "../schemas/JavaDomainDay.js";
import { JavaDomainHour } from "../schemas/JavaDomainHour.js";
import { Selector } from "../services/DB/Selector.js";
import { dateToString } from "./utils/dateToString.js";


export class ComputeDomainDay {
  ads: any[] = [];
  campaigns = new Map<number, any>();

  constructor() {
    this.init();
  }

  private async init() {
    const nowDate = new Date();
    const startDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 1);
    const endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 1);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();
    const start = dateToString(startDate);
    const end = dateToString(endDate);
    console.log(startTS, endTS);
    let banner = await Banner.find({$and: [{created: {$gte: startTS, $lt: endTS}}]}).count();
    console.log('banner', banner);

    let list = await JavaDomainDay.find({start: startTS, end: endTS});
    console.log('Obliczeń', list.length);
    let sum = 0;
    list.forEach(x => sum += x.banner!);
    console.log('sum', sum);

    const domains = await new Selector.Builder()
      .setSql('SELECT id FROM business_domain')
      .build()
      .getMany();
    const domainsIds = domains.map(domain => domain.id);
    console.log('Domen', domainsIds.length)
    banner = await Banner.find({created: {$gte: startTS, $lt: endTS}, domain: {$in: domainsIds}}).count();
    console.log('banner2', banner);

    list = await JavaDomainDay.find({start: startTS, end: endTS, domain: {$in: domainsIds}});
    sum = 0;
    list.forEach(x => sum += x.banner!);
    console.log('sum2', sum);
  }
}
