import { Banner } from "../schemas/Banner.js";
import { BannerAdded } from "../schemas/BannerAdded.js";
import { BannerClicked } from "../schemas/BannerClicked.js";
import { BannerLoaded } from "../schemas/BannerLoaded.js";
import { BannerShown } from "../schemas/BannerShown.js";
import { BannerlessSection } from "../schemas/BannerlessSection.js";
import { DomainDay } from "../schemas/DomainDay.js";
import { NodeDomainHour } from "../schemas/NodeDomainHour.js";
import { ScriptDownloaded } from "../schemas/ScriptDownloaded.js";
import { ScriptLoaded } from "../schemas/ScriptLoaded.js";
import { Selector } from "../services/DB/Selector.js";
import { ExtendedArray } from "../services/ExtendedArray/ExtendedArray.js";
import { dateToString } from "./utils/dateToString.js";


export class ComputeNodeDomainHour2 {
  ads: any[] = [];
  campaigns = new Map<number, any>();

  constructor() {
    this.init();
  }

  private async init() { 
    // wyszukiwam timestamp poprzedniej godziny jako startTS i timestamp dla startTS + 1 godzina
    const nowDate = new Date();
    const startDate = new Date(2024, 1, 16, nowDate.getHours() - 1);
    const endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours() + 2);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();
    const start = dateToString(startDate);
    const end = dateToString(endDate);

    // Pobieram listę domen
    const domains = await new Selector.Builder()
      .setSql('SELECT id FROM business_domain')
      .build()
      .getMany();
    const domainsIds = domains.map(domain => domain.id);
    const extendedDomainsIds = new ExtendedArray(...domainsIds);
    const filter = {created: {$gte: startTS, $lt: endTS}};

    // początek pobrania wszystkich metryk którę potrzebuję
    let banners = await Banner.find(filter).exec();
    console.log('banners:', banners.length);

    let bannersAdded = await BannerAdded.find(filter).exec();
    console.log('bannersAdded:', bannersAdded.length);

    let bannersLoaded = await BannerLoaded.find(filter).exec();
    console.log('bannersLoaded:', bannersLoaded.length);

    let bannersShown = await BannerShown.find(filter).exec();
    console.log('bannersShown:', bannersShown.length);

    let bannersClicked = await BannerClicked.find(filter).exec();
    console.log('bannersClicked:', bannersClicked.length);

    let bannerlessSections = await BannerlessSection.find(filter).exec();
    console.log('bannerlessSections:', bannerlessSections.length);

    let scriptsLoaded = await ScriptLoaded.find(filter).exec();
    console.log('scriptsLoaded:', scriptsLoaded.length);

    let scriptsDownloaded = await ScriptDownloaded.find(filter).exec();
    console.log('scriptsDownloaded:', scriptsDownloaded.length);
    // koniec pobrania metryk

    await extendedDomainsIds.asyncForEach(async (id, idx) => { // iteruję się po każdej DOMENIE
      console.log(idx, '/', domainsIds.length);
      const s = Date.now(); // to dla obliczenia czasu, ile czasu zawiera parsowania metryk dla jednej domeny
      const counters: {[k: string]: number} = { // liczniki, na początku są same 0
        banner: 0,
        banner_added: 0,
        banner_loaded: 0,
        banner_shown: 0,
        banner_clicked: 0,
        bannerless_section: 0,
        script_loaded: 0,
        script_downloaded: 0,
      }

      /*
      żeby nie kopiować kod - zrobiłęm funkcję. W javie ich nie ma to zamiast tego by była prywatna metoda:

      private void cb(String key, Document[] list, Map<String, int> counters)

      jak widać pojawił się dodatkowy parametr "counters", żeby Java miała dostęp
      */
      const cb = (key: string, list: {domain?: number}[]) => { 
        // przechodzę po metrykam i jak znajdę metrykę dla tej domeny, zwiększam odpowiedni licznik + 1 (parametr "key")
        for (let i = 0, l = list.length; i < l; i++) {
          if (list[i].domain !== id) continue;
          ++counters[key];
        }
      }

      /*
      Wywołuję metodę dla każdej kolekcji metryk. W Javie to będzie np tak:
      cb('banner', banners, counters);
      */
      cb('banner', banners);
      cb('banner_added', bannersAdded);
      cb('banner_loaded', bannersLoaded);
      cb('banner_shown', bannersShown);
      cb('banner_clicked', bannersClicked);
      cb('bannerless_section', bannerlessSections);
      cb('script_loaded', scriptsLoaded);
      cb('script_downloaded', scriptsDownloaded);

      /*
      tworzymy wpis
      ...counters znaczy że skopiuj tutaj cały objekt, np

      const a = {
        b: 1
      }

      const c = {
        ...a,
        d: true
      }

      console.log(c);

      Wynik:

      {
        b: 1,
        d: true
      }
      */
      const nodeDomainHour = new NodeDomainHour({
        ...counters,
        domain: id,
        start: startTS,
        end: endTS,
      });

      await nodeDomainHour.save(); // zapisuję go do bazy
      console.log(`${(Date.now() - s) / 1000}s`); // wyświetlam czas na obliczenia dla 1 domeny
      console.log('----------');
    });

    if (startDate.getDay() !== endDate.getDay()) {
      await this.computeDomainDayResults(
        new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDay()),
        new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDay())
      );
    }
  }

  async computeDomainDayResults(startDate: Date, endDate: Date) {
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();
    const list = await NodeDomainHour.find({start: {$gte: startTS}, end: {$lt: endTS}}).exec();
    const domains = new ExtendedArray<number>(...list.map(x => x.domain!));

    await domains.asyncForEach(async id => {
      let banner = 0;
      // i inne metryki, nie piszę ich póki dla uproszczenia

      list
        .filter(x => x.domain === id)
        .forEach(x => banner += x.banner!);

      const data = new DomainDay({
        banner,
        // reszta metryk
        start: startTS,
        end: endTS,
        domain: id,
      });

      await data.save();
    });

    if (startDate.getMonth() !== endDate.getMonth()) {
      await this.computeDomainMonthResults(
        new Date(startDate.getFullYear(), startDate.getMonth()),
        new Date(endDate.getFullYear(), endDate.getMonth())
      );
    }
  }

  async computeDomainMonthResults(startDate: Date, endDate: Date) {
    // jak w poprzedniem przykładzie tylko dla miesiąca
  }
}