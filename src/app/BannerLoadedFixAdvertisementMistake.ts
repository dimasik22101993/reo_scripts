import { BannerLoaded } from '../schemas/BannerLoaded.js';
import * as mongoose from 'mongoose';


export class BannerLoadedFixAdvertisementMistake {
  filter = {advertisement: 0, ad_file: {$gt: 0}};
  count = 0;

  constructor() {
    this.init();
  }

  private async init() {
    this.count = await BannerLoaded.find(this.filter).count();
    console.log(`Count: ${this.count}`);
    const cursor = BannerLoaded.find(this.filter).cursor();

    cursor.on('data', async (doc) => {
      doc.advertisement = doc.ad_file;
      doc.ad_file = 0;
      await doc.save();
    });
    
    cursor.on('end', () => {
      console.log('DONE');
      mongoose.connection.close();
    });
  }
}