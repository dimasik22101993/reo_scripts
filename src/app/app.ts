import { BannerLoadedFixAdvertisementMistake } from './BannerLoadedFixAdvertisementMistake.js';
import { BannerShownFixAdvertisementMistake } from './BannerShownFixAdvertisementMistake.js';
import { ComputeAdFileDomainDay } from './ComputeAdFileDomainDay.js';
import { ComputeCampaignHour } from './ComputeCampaignHour.js';
import { ComputeDomainCampaignDay } from './ComputeDomainCampaignDay.js';
import { ComputeDomainDay } from './ComputeDomainDay.js';
import { ComputeDomainHour } from './ComputeDomainHour.js';
import { ComputeIPCounters } from './ComputeIPCounters.js';
import { ComputeNodeDomainHour } from './ComputeNodeDomainHour.js';
import { ComputeNodeDomainHour2 } from './ComputeNodeDomainHour2.js';
import { RemoveOldMetrics } from './RemoveOldMetrics.js';
import { Script1305 } from './Script1305.js';
import { Script1505 } from './Script1505.js';


export default class App {
  
  constructor() {
    new ComputeAdFileDomainDay();
  }
}
