import { Banner } from "../schemas/Banner.js";
import { BannerShown } from "../schemas/BannerShown.js";
import { DomainDay } from "../schemas/DomainDay.js";
import { JavaDomainDay } from "../schemas/JavaDomainDay.js";
import { JavaDomainHour } from "../schemas/JavaDomainHour.js";
import { ScriptDownloaded } from "../schemas/ScriptDownloaded.js";
import { Selector } from "../services/DB/Selector.js";
import { ExtendedArray } from "../services/ExtendedArray/ExtendedArray.js";
import { MS_IN_DAY } from "./constants/constants.js";
import { dateToString } from "./utils/dateToString.js";
import * as fs from 'fs';


export class ComputeIPCounters {
  ads: any[] = [];
  campaigns = new Map<number, any>();
  ips: {[k: string]: number} = {};
  files: {[k: string]: number} = {};

  constructor() {
    // this.init();
    // this.compute();
    // const ipsStr = fs.readFileSync(`ips0605.json`, {encoding: 'utf-8'});
    // const ips: {[k: string]: number} = JSON.parse(ipsStr);
    // console.log(Object.keys(ips).length);
    this.init2();

  }

  // private async init2() {
  //   const banners = await Banner
  //     .find({
  //       advertisement: 18
  //     })
  //     .exec();
      
  //     const domains: {[k: string]: number} = {};

  //     banners.forEach(banner => {
  //       const k = (banner.domain || 0).toString();
  //       if (!domains[k]) {
  //         domains[k] = 1;
  //       } else {
  //         ++domains[k];
  //       }
  //     });

  //     fs.writeFileSync('domains0605.json', JSON.stringify(domains, null, 2));
  //     console.log('DONE')
  // }

  private async init2() {
    const allDomainsStr = fs.readFileSync('all_domains.json', 'utf-8');
    const domainsStr = fs.readFileSync('domains0605.json', 'utf-8');
    const domains: {[k: string]: number} = JSON.parse(domainsStr);
    const allDomains: any[] = JSON.parse(allDomainsStr);
    const list: [string, string, string][] = [];

    Object.entries(domains).forEach(([k, v]) => {
      const domain = allDomains.find(d => d.id == k);
      list.push([k, domain.domain, v.toString()]);
    });

    fs.writeFileSync('domains0605.csv', list.map(x => x.map(y => `"${y}"`).join(',')).join('\n'));
    console.log('DONE')
  }

  // private compute() {
  //   for (let i = 1; i <= 23; i++) {
  //     console.log(i, '/', 23);
  //     const ipsStr = fs.readFileSync(`ips0605-${i}.json`, {encoding: 'utf-8'});
  //     const filesStr = fs.readFileSync(`files0605-${i}.json`, {encoding: 'utf-8'});

  //     const ips: {[k: string]: number} = JSON.parse(ipsStr);
  //     const files: {[k: string]: number} = JSON.parse(filesStr);

  //     Object.entries(ips).forEach(([k, v]) => {
  //       if (!this.ips[k]) this.ips[k] = 0;
  //       this.ips[k] += v;
  //     });

  //     Object.entries(files).forEach(([k, v]) => {
  //       if (!this.files[k]) this.files[k] = 0;
  //       this.files[k] += v;
  //     });
  //   }

  //   fs.writeFileSync('ips0605.json', JSON.stringify(this.ips, null, 2));
  //   fs.writeFileSync('files0605.json', JSON.stringify(this.files, null, 2));
  // }

  // private async init() {
  //   const x = fs.readFileSync(`ipCounters.json`, {encoding: 'utf-8'});
  //   const ipCounters: {[k: string] : number} = JSON.parse(x);
  //   const top100 = Object.entries(ipCounters)
  //     .sort((a, b) => {
  //       const av = a[1];
  //       const bv = b[1];

  //       if (av > bv) return -1;
  //       if (av < bv) return 1;
  //       return 0;
  //     })
  //     .map(([k, v]) => {
  //       return {
  //         ip: k,
  //         counter: v,
  //       }
  //     })
  //     .filter((_, i) => i < 100);

  //   fs.writeFileSync('top100.json', JSON.stringify(top100, null, 2));
  //   console.log('done')
  // }

  // private async init() {
  //   const ips = new Set<string>();
  //   const ipCounters: {[k: string] : number} = {};

  //   for (let i = 1; i <=31; i++) {
  //     console.log(i)
  //     const x = fs.readFileSync(`ps-${i}.json`, {encoding: 'utf-8'});
  //     const y = fs.readFileSync(`ipCounters-${i}.json`, {encoding: 'utf-8'});
  //     const _ips: string[] = JSON.parse(x);
  //     const _ipCounters: {[k: string] : number} = JSON.parse(y);
  //     _ips.forEach(ip => ips.add(ip));
  //     Object.entries(_ipCounters).forEach(([k, v]) => {
  //       const counter = ipCounters[k];
  //       if (counter) {
  //         ipCounters[k] += v;
  //       } else {
  //         ipCounters[k] = v;
  //       }
  //     });
  //   }

  //   fs.writeFileSync('ips.json', JSON.stringify(ips, null, 2));
  //   fs.writeFileSync('ipCounters.json', JSON.stringify(ipCounters, null, 2));
  // }

  // private async init() {
  //   const ipsStr = fs.readFileSync(`ips2504.json`, 'utf-8');
  //   const filesStr = fs.readFileSync(`files2504.json`, 'utf-8');

  //   const ips = JSON.parse(ipsStr);
  //   const files = JSON.parse(filesStr);

  //   console.log('ips', Object.keys(ips).length);

  //   const result: {[k: string]: number} = {}

  //   Object.values(files).forEach((file: any) => {
  //     const { size, count } = file;
  //     if (!result[size]) {
  //       result[size] = count;
  //     } else {
  //       result[size] += count;
  //     }
  //   });

  //   console.log(result)
  // }

  private async init() {
    const startDate = new Date(2024, 3, 18);
    const endDate = new Date(2024, 4, 7);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();

    const stepSize = 1_000_000;
    let skip = 0;

    let ips: {[k: string]: number} = {};
    let files: {[k: string]: number} = {};

    const bannerCounter = await BannerShown
      .find(
        {created: {$gte: startTS, $lt: endTS}}, 
        'ip ad_file advertisement'
      )
      .count();
    console.log('bannerCounter', bannerCounter);

    const w = async () => {
      if (skip >= bannerCounter) return;
      console.log(skip, '/', bannerCounter);
      const banners = await BannerShown
        .find(
          {created: {$gte: startTS, $lt: endTS}}, 
          'ip ad_file advertisement'
        )
        .skip(skip)
        .limit(stepSize)
        .find();

      if (!banners.length) return;
      console.log(banners.length);

      banners.forEach((banner, i) => {
        if (![16, 17, 18].includes(banner.advertisement!)) return;
  
        const { ip, ad_file } = banner;
        if (ip) {
          if (!ips[ip]) {
            ips[ip] = 1;
          } else {
            ++ips[ip];
          }
        }
  
        if (ad_file) {
          const k = ad_file.toString();
          if (!files[k]) {
            files[k] = 1;
          } else {
            ++files[k];
          }
        }
      });

      skip += stepSize;

      fs.writeFileSync(`ips0605-${skip / stepSize}.json`, JSON.stringify(ips, null, 2));
      fs.writeFileSync(`files0605-${skip / stepSize}.json`, JSON.stringify(files, null, 2));
      ips = {};
      files = {};

      await w();
    }

    await w();

    // fs.writeFileSync(`ips0605.json`, JSON.stringify(ips, null, 2));
    // fs.writeFileSync(`files0605.json`, JSON.stringify(files, null, 2));
    console.log('THE END');
  }

  // private async init() {
  //   const startDate = new Date(2024, 2, 1);
  //   const endDate = new Date(2024, 3, 1);
  //   const ips = new Set<string>();
  //   const ipCounters: {[k: string] : number} = {};

  //   const startTS = startDate.getTime();
  //   const endTS = endDate.getTime();
  //   const start = dateToString(startDate);
  //   const end = dateToString(endDate);
  //   const days: number[] = [];
  //   let n = startTS;

  //   while (n < endTS) {
  //     days.push(n);
  //     n = n + MS_IN_DAY;
  //   }

  //   // 1709247600084
  //   // const x = await ScriptDownloaded
  //   //     .findOne({$and: [{created: 1709247600084}]}, 'ip')
  //   //     .lean()
  //   //     .exec();
  //   // console.log(x)

  //   console.log(startTS, endTS);
  //   console.log('-------')
  //   // let count = await ScriptDownloaded
  //   //   .find({$and: [{created: {$gte: startTS, $lt: endTS}}]})
  //   //   .count();
  //   //   console.log(count)

  //   let i = 0;
  //   const startIdx = 18;
  //   for (const d of days) {
  //     if (i < startIdx) {
  //       ++i;
  //       continue;
  //     }
  //     const s = new Date().getTime();
  //     console.log((i + 1) + ' / ' + days.length);
  //     console.log({$gte: d, $lt: d + MS_IN_DAY})
  //     let scripts = await ScriptDownloaded
  //       .find({$and: [{created: {$gte: d, $lt: d + MS_IN_DAY}}]}, 'ip')
  //       .lean()
  //       .exec();

  //       const _ips = new Set<string>();
  //       scripts.forEach(script => {
  //         const ip = script.ip || '-';
  //         _ips.add(ip);
  //         if (!ipCounters[ip]) {
  //           ipCounters[ip] = 1;
  //         } else {
  //           ++ipCounters[ip];
  //         }
  //       });

  //     console.log(_ips.size);
  //     console.log(scripts.length)
  //     console.log('Taken: ', ((Date.now() - s) / 1000).toFixed(3), 's');
      
  //     _ips.forEach(ip => ips.add(ip));
  //     ++i;

  //     fs.writeFileSync(`ps-${i}.json`, JSON.stringify(Array.from(ips), null, 2));
  //     fs.writeFileSync(`ipCounters-${i}.json`, JSON.stringify(ipCounters, null, 2));
  //   }

  //   console.log(Array.from(ips).length);
  //   fs.writeFileSync('ips.json', JSON.stringify(Array.from(ips), null, 2));
  //   fs.writeFileSync('ipCounters.json', JSON.stringify(ipCounters, null, 2));

  //   // console.log(startTS, endTS);
  //   // console.log('-------')
  //   // let scripts = await ScriptDownloaded
  //   //   .find({$and: [{created: {$gte: startTS, $lt: endTS}}]}, 'ip')
  //   //   .lean()
  //   //   .exec();
  //   //   console.log(scripts.length)
  // }
}
