export function dateToString(date: Date): string {
  let [dateStr, timeStr] = date.toLocaleString().split(', ');
  dateStr = dateStr.split('.').reverse().join('-');
  return `${dateStr} ${timeStr}`;
}
