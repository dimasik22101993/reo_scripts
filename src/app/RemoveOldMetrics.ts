import { Banner } from "../schemas/Banner.js";
import { BannerAdded } from "../schemas/BannerAdded.js";
import * as fs from 'fs';
import { BannerLoaded } from "../schemas/BannerLoaded.js";
import { BannerShown } from "../schemas/BannerShown.js";
import { BannerClicked } from "../schemas/BannerClicked.js";
import { BannerlessSection } from "../schemas/BannerlessSection.js";

export class RemoveOldMetrics {

  constructor() {
    this.init();
  }

  async init() {
    const time = new Date(2024, 1, 1).getTime();
    const filter = {created: {$lt: time}};
    const x = await BannerlessSection.find(filter).count();
    console.log(x);
    // let start = Date.now();
    // fs.writeFileSync('banner_added.json', JSON.stringify(bannerAdded));
    // console.log(`${(Date.now() - start) / 1000} s`);
  }
}