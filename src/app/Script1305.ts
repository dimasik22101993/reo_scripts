import { Banner } from "../schemas/Banner.js";
import { BannerShown } from "../schemas/BannerShown.js";
import * as fs from 'fs';


export class Script1305 {
  days: {
    start: number,
    end: number,
    day: string,
  }[] = [];

  constructor() {
    this.init();
  }

  private async init() {
    // await this.downloadData();
    this.createDays();
    // this.compute1();
    // this.compute2();
    // this.compute3();
    this.compute4();
    // this.downloadBanners();
  }

  private async downloadData() {
    const startDate = new Date(2024, 3, 18);
    const endDate = new Date(2024, 4, 8);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();

    const stepSize = 1_000_000;
    let skip = 0;

    const bannerCounter = await BannerShown
      .find(
        {created: {$gte: startTS, $lt: endTS}}, 
      )
      .count();
    console.log('bannerCounter', bannerCounter);

    const w = async () => {
      if (skip >= bannerCounter) return;
      console.log(skip, '/', bannerCounter);
      let banners = await BannerShown
        .find(
          {created: {$gte: startTS, $lt: endTS}}, 
        )
        .skip(skip)
        .limit(stepSize)
        .find();

      if (!banners.length) return;
      console.log(banners.length);

      skip += stepSize;

      fs.writeFileSync(`banner_shown_${skip - stepSize}-${Math.min(skip, bannerCounter)}.json`, JSON.stringify(banners, null, 2));
      banners = [];
      await w();
    }

    await w();
    console.log('THE END');
  }

  private async downloadBanners() {
    console.log('downloadBanners')
    const startDate = new Date(2024, 3, 22);
    const endDate = new Date(2024, 4, 1);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();

    const stepSize = 1_000_000;
    let skip = 0;

    const bannerCounter = await Banner
      .find(
        {created: {$gte: startTS, $lt: endTS}, advertisement: {$in: [17, 18]}}, 
      )
      .count();
    console.log('bannerCounter', bannerCounter);

    const w = async () => {
      if (skip >= bannerCounter) return;
      console.log(skip, '/', bannerCounter);
      let banners = await Banner
        .find(
          {created: {$gte: startTS, $lt: endTS}, advertisement: {$in: [17, 18]}}, 
        )
        .skip(skip)
        .limit(stepSize)
        .find();

      if (!banners.length) return;
      console.log(banners.length);

      skip += stepSize;

      fs.writeFileSync(`banner_${skip - stepSize}-${Math.min(skip, bannerCounter)}.json`, JSON.stringify(banners, null, 2));
      banners = [];
      await w();
    }

    await w();
    console.log('THE END');
  }

  createDays() {
    // const startDate = new Date(2024, 3, 18);
    // const endDate = new Date(2024, 4, 8);
    const startDate = new Date(2024, 3, 22);
    const endDate = new Date(2024, 4, 1);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();

    let n = startTS;

    while (n < endTS) {
      const day = this.getDayByTimestamp(n);
      const end = n + 1000 * 60 * 60 * 24;
      this.days.push({
        start: n,
        end,
        day,
      });

      n = end;
    }

    console.log(this.days);
  }

  // {
  //   "_id": "662046a4f4cf5d31ff1ce223",
  //   "origin": "https://tko.pl",
  //   "pathname": "/233059,2024,03,22,wiksons-group-opinie-dokonaj-swojej-pierwszej-inwestycji-juz-dzis",
  //   "domain": 814,
  //   "created": 1713391268976,
  //   "ip": "83.26.245.56",
  //   "advertisement": 9,
  //   "ad_file": 22
  // },

  private compute1() {
    const STEP = 1000000;
    let bannersStr: string;
    let banners: any[];

    const result: {
      [day: string]: {
        [ip: string]: number,
      }
    } = {};

    for (let i = 0; i < 25; i++) {
      console.log(i + 1, '/', 25);
      const n1 = i * STEP;
      const n2 = n1 + STEP;
      bannersStr = fs.readFileSync(`banner_shown_${n1}-${n2}.json`, 'utf-8');
      banners = JSON.parse(bannersStr);
      
      for (let j = 0, l = banners.length; j < l; j++) {
        const banner = banners[j];
        const created: number = banner.created;
        if (!created) continue;

        const day = this.getDayByCreated(created);
        let dailyResult = result[day];
        if (!dailyResult) {
          dailyResult = {};
          result[day] = dailyResult;
        }

        if (!dailyResult[banner.ip]) {
          dailyResult[banner.ip] = 0;
        } 

        ++dailyResult[banner.ip];
      }
    }

    fs.writeFileSync('result.json', JSON.stringify(result, null, 2));
  }

  private async compute2() {
    const file = fs.readFileSync('advertisements.json', 'utf-8');
    const advertisements: {
      id: string,
      ad_file: string,
      ad_dimension: string,
      dimension_name: string,
      campaign: string,
      advertisement: string,
    }[] = JSON.parse(file);

    const STEP = 1000000;
    let bannersStr: string;
    let banners: any[];

    // const result: {
    //   id: string,
    //   ad_dimension: string,
    //   dimension_name: string,
    //   campaign: string,
    //   advertisement: string,
    //   counter: number,
    // }[] = [];

    const result: {
      [k: string]: {
        id: string,
        ad_dimension: string,
        dimension_name: string,
        campaign: string,
        advertisement: string,
        counter: number,
      }
    } = {};

    for (let i = 0; i < 25; i++) {
      console.log(i + 1, '/', 25);
      const n1 = i * STEP;
      const n2 = n1 + STEP;
      bannersStr = fs.readFileSync(`banner_shown_${n1}-${n2}.json`, 'utf-8');
      banners = JSON.parse(bannersStr);
      
      for (let j = 0, l = banners.length; j < l; j++) {
        const banner = banners[j];
        let adFileId: string = (banner.ad_file || 0).toString();
        let x = result[adFileId];
        
        if (!x) {
          if (!(+adFileId)) {
            x = {
              id: '?',
              ad_dimension: '?',
              dimension_name: '?',
              campaign: '?',
              advertisement: '?',
              counter: 0,
            }
          } else {
            const advertisement = advertisements.find(y => +y.id === +adFileId);

            if (!advertisement) {
              x = {
                id: '?',
                ad_dimension: '?',
                dimension_name: '?',
                campaign: '?',
                advertisement: '?',
                counter: 0,
              }
            } else {
              x = {
                ...advertisement,
                counter: 0,
              }
            }
          }

          result[adFileId] = x;
        } 

        ++result[adFileId].counter;
      }
    }

    fs.writeFileSync('result2.json', JSON.stringify(result, null, 2));
  }

  private compute3() {
    const STEP = 1000000;
    let bannersStr: string;
    let banners: any[];

    const result: {
      [day: string]: {
        [domain: string]: number,
      }
    } = {};

    for (let i = 0; i < 2; i++) {
      console.log(i + 1, '/', 2);
      const n1 = i * STEP;
      const n2 = n1 + STEP;
      bannersStr = fs.readFileSync(`banner_${n1}-${n2}.json`, 'utf-8');
      banners = JSON.parse(bannersStr);
      
      for (let j = 0, l = banners.length; j < l; j++) {
        const banner = banners[j];
        const created: number = banner.created;
        if (!created) continue;

        const day = this.getDayByCreated(created);
        let dailyResult = result[day];
        if (!dailyResult) {
          dailyResult = {};
          result[day] = dailyResult;
        }

        if (!dailyResult[banner.origin]) {
          dailyResult[banner.origin] = 0;
        } 

        ++dailyResult[banner.origin];
      }
    }

    fs.writeFileSync('result3.json', JSON.stringify(result, null, 2));
  }

  private compute4() {
    const STEP = 1000000;
    let bannersStr: string;
    let banners: any[];

    const result: {
      [day: string]: {
        [domain: string]: number,
      }
    } = {};

    for (let i = 0; i < 2; i++) {
      console.log(i + 1, '/', 2);
      const n1 = i * STEP;
      const n2 = n1 + STEP;
      bannersStr = fs.readFileSync(`banner_${n1}-${n2}.json`, 'utf-8');
      banners = JSON.parse(bannersStr);
      
      for (let j = 0, l = banners.length; j < l; j++) {
        const banner = banners[j];
        const origin: string = banner.origin || '';

        let originResult = result[origin];
        if (!originResult) {
          originResult = {};
          result[origin] = originResult;
        }

        const day = this.getDayByCreated(banner.created);

        if (!originResult[day]) {
          originResult[day] = 0;
        } 

        ++originResult[day];
      }
    }

    fs.writeFileSync('result4.json', JSON.stringify(result, null, 2));
  }

  getDayByTimestamp(created: number) {
      const createdData = new Date(created);
      const Y = createdData.getFullYear();
      const m = createdData.getMonth() + 1;
      const d = createdData.getDate();

      const M = m > 9 ? m : '0' + m;
      const D = d > 9 ? d : '0' + d;

      return `${Y}-${M}-${D}`;
  }

  getDayByCreated(created: number) {
    const x = this.days.find(d => d.start <= created && d.end > created);
    if (!x) throw new Error();
    return x.day;
  }
}
