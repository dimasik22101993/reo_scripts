import { Banner } from "../schemas/Banner.js";
import { BannerAdded } from "../schemas/BannerAdded.js";
import { BannerClicked } from "../schemas/BannerClicked.js";
import { BannerLoaded } from "../schemas/BannerLoaded.js";
import { BannerShown } from "../schemas/BannerShown.js";
import { CampaignHour } from "../schemas/CampaignHour.js";
import { JavaCampaignHour } from "../schemas/JavaCampaignHour.js";
import { Selector } from "../services/DB/Selector.js";
import { ExtendedArray } from "../services/ExtendedArray/ExtendedArray.js";
import { dateToString } from "./utils/dateToString.js";


export class ComputeCampaignHour {
  ads: any[] = [];
  campaigns = new Map<number, any>();

  constructor() {
    this.init();
  }

  private async init() {
    const nowDate = new Date();
    const startDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), nowDate.getHours() - 1);
    const endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours() + 1);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();
    const start = dateToString(startDate);
    const end = dateToString(endDate);

    console.log(start, end)
    console.log(startTS, endTS)
    this.ads = [
      { campaign: 9, advertisement: 8 },
      { campaign: 11, advertisement: 9 },
      { campaign: 15, advertisement: 13 },
    ]
    console.log(this.ads)
    this.ads.forEach(ad => {
      this.campaigns.set(ad.campaign, {
        banner: 0,
        banner_added: 0,
        banner_loaded: 0,
        banner_shown: 0,
        banner_clicked: 0,
      });
    });

    const campaignsIds = ExtendedArray.from(this.campaigns.keys()) as ExtendedArray<number>;
    const filter = {created: {$gte: startTS, $lt: endTS}, advertisement: {$in: []}};

    const sum = {
      banner: 0,
      banner_added: 0,
      banner_loaded: 0,
      banner_shown: 0,
      banner_clicked: 0,
    }

    await campaignsIds.asyncForEach(async id => {
      console.log('id', id);
      const adIds = this.ads.filter(ad => ad.campaign === id).map(ad => ad.advertisement);
      console.log('adIds', adIds);
      if (!adIds) return;

      const banner = await Banner.find({
        ...filter,
        advertisement: {
          $in: adIds,
        },
      }).count();
      console.log('banner', banner);

      const banner_added = await BannerAdded.find({
        ...filter,
        advertisement: {
          $in: adIds,
        },
      }).count();
      console.log('banner_added', banner_added);

      const banner_loaded = await BannerLoaded.find({
        ...filter,
        advertisement: {
          $in: adIds,
        },
      }).count();
      console.log('banner_loaded', banner_loaded);

      const banner_shown = await BannerShown.find({
        ...filter,
        advertisement: {
          $in: adIds,
        },
      }).count();
      console.log('banner_shown', banner_shown);

      const banner_clicked = await BannerClicked.find({
        ...filter,
        advertisement: {
          $in: adIds,
        },
      }).count();
      console.log('banner_clicked', banner_clicked);

      this.campaigns.set(id, {
        banner,
        banner_added,
        banner_loaded,
        banner_shown,
        banner_clicked,
      });

      sum.banner += banner;
      sum.banner_added += banner_added;
      sum.banner_loaded += banner_loaded;
      sum.banner_shown += banner_shown;
      sum.banner_clicked += banner_clicked;
    });

    console.log('sum', sum);

    const sum2 = {
      banner: 0,
      banner_added: 0,
      banner_loaded: 0,
      banner_shown: 0,
      banner_clicked: 0,
    }

    const campaigns = await JavaCampaignHour.find({start: startTS, end: endTS});
    campaigns.forEach(campaign => {
      console.log(campaign);

      sum2.banner += campaign.banner!;
      sum2.banner_added += campaign.banner_added!;
      sum2.banner_loaded += campaign.banner_loaded!;
      sum2.banner_shown += campaign.banner_shown!;
      sum2.banner_clicked += campaign.banner_clicked!;
    });

    console.log('sum2', sum2);
  }
}