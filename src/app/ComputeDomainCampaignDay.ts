import { Banner } from "../schemas/Banner.js";
import { BannerAdded } from "../schemas/BannerAdded.js";
import { BannerClicked } from "../schemas/BannerClicked.js";
import { BannerLoaded } from "../schemas/BannerLoaded.js";
import { BannerShown } from "../schemas/BannerShown.js";
import { BannerlessSection } from "../schemas/BannerlessSection.js";
import { DomainCampaignDay } from "../schemas/DomainCampaignDay.js";
import { ADS, MS_IN_DAY } from "./constants/constants.js";


interface Campaign {
  id: number,
  ads: number[],
}

export class ComputeDomainCampaignDay {
  campaigns = new Map<number, number[]>(); // klucz to id kampanii, wartosć tablica reklam kampanii 
  adCampaign = new Map<number, number>(); // klucz to id reklamy, wartość to id kampanii 

  constructor() {
    this.init();
  }

  async init() {
    this.defineCampaigns();
    this.defineAdCampaigns();
    this.compute();
  }

  private defineCampaigns() {
    ADS.forEach(ad => {
      const { id, campaign: campaignId } = ad;
      if (!this.campaigns.get(campaignId)) this.campaigns.set(campaignId, []);
      this.campaigns.get(campaignId)!.push(id);
    });
  }

  private defineAdCampaigns() {
    ADS.forEach(ad => {
      this.adCampaign.set(ad.id, ad.campaign);
    });
  }

  private async compute() {
    let n = 1714514400000; //new Date(2024, 3, 1).getTime();
    const end = 1716501600000;

    while (n < end) {
      const domainCampaignDays: {[k: string]: any} = {};
      console.log('---');
      console.log(n);
      const start = Date.now();
      const nextDay = n + MS_IN_DAY;
      const config = {
        created: {
          $gte: n, $lt: nextDay
        }
      }

      await this.computeBanners({
        config,
        domainCampaignDays,
        start: n,
        end: nextDay,
        model: Banner,
        modelName: 'banner',
      });

      await this.computeBanners({
        config,
        domainCampaignDays,
        start: n,
        end: nextDay,
        model: BannerAdded,
        modelName: 'banner_added',
      });

      await this.computeBanners({
        config,
        domainCampaignDays,
        start: n,
        end: nextDay,
        model: BannerClicked,
        modelName: 'banner_clicked',
      });

      await this.computeBanners({
        config,
        domainCampaignDays,
        start: n,
        end: nextDay,
        model: BannerLoaded,
        modelName: 'banner_loaded',
      });

      await this.computeBanners({
        config,
        domainCampaignDays,
        start: n,
        end: nextDay,
        model: BannerShown,
        modelName: 'banner_shown',
      });

      await this.computeBanners({
        config,
        domainCampaignDays,
        start: n,
        end: nextDay,
        model: BannerlessSection,
        modelName: 'bannerless_section',
      });

      const list = Object.values(domainCampaignDays);

      for (let x of list) {
        await x.save();
      }

      n = nextDay;
      console.log((Date.now() - start) / 1000, 's');
    }
  }

  private async computeBanners(data: {
    config: any,
    domainCampaignDays: any,
    start: number,
    end: number,
    model: any,
    modelName: string,
  }) {
    let skip = 0;
    const limit = 1_000_000;
    
    while (true) {
      const banners: any[] = await data.model
      .find(data.config)
      .skip(skip)
      .limit(limit)
      .exec();
      console.log('skip', skip);
      console.log(data.modelName, banners.length);
      banners.forEach((banner: any) => {
        const adId = banner.advertisement;
        if (!adId) return;

        const domainId = banner.domain;
        const campaignId = this.adCampaign.get(adId);
        if (!campaignId || !domainId) return;

        const key = domainId + '_' + campaignId;

        this.setDomainCampaignDays(data.domainCampaignDays, {
          campaign: campaignId,
          domain: domainId,
          start: data.start,
          end: data.end,
        });

        ++data.domainCampaignDays[key][data.modelName];
      });

      if (banners.length >= limit) {
        skip += limit;
        continue;
      }

      break;
    }
  }

  private setDomainCampaignDays(domainCampaignDays: {[k: string]: any}, data: {
    campaign: number,
    domain: number,
    start: number,
    end: number,
  }) {
    const domainId = data.domain;
    const campaignId = data.campaign
    const key = domainId + '_' + campaignId;

    if (!domainCampaignDays[key]) {
      domainCampaignDays[key] = new DomainCampaignDay({
        ...data,
        banner: 0,
        banner_added: 0,
        banner_clicked: 0,
        banner_loaded: 0,
        banner_shown: 0,
        bannerless_section: 0,
      });
    }
  }
}