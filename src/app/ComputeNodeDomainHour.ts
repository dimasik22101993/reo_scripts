import { Banner } from "../schemas/Banner.js";
import { BannerAdded } from "../schemas/BannerAdded.js";
import { BannerClicked } from "../schemas/BannerClicked.js";
import { BannerLoaded } from "../schemas/BannerLoaded.js";
import { BannerShown } from "../schemas/BannerShown.js";
import { BannerlessSection } from "../schemas/BannerlessSection.js";
import { NodeDomainHour } from "../schemas/NodeDomainHour.js";
import { ScriptDownloaded } from "../schemas/ScriptDownloaded.js";
import { ScriptLoaded } from "../schemas/ScriptLoaded.js";
import { Selector } from "../services/DB/Selector.js";
import { ExtendedArray } from "../services/ExtendedArray/ExtendedArray.js";
import { dateToString } from "./utils/dateToString.js";


export class ComputeNodeDomainHour {
  ads: any[] = [];
  campaigns = new Map<number, any>();

  constructor() {
    this.init();
  }

  private async init() {
    const nowDate = new Date();
    const startDate = new Date(2024, 1, 16, nowDate.getHours() - 1);
    const endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours() + 2);
    const startTS = startDate.getTime();
    const endTS = endDate.getTime();
    const start = dateToString(startDate);
    const end = dateToString(endDate);

    const domains = await new Selector.Builder()
      .setSql('SELECT id FROM business_domain')
      .build()
      .getMany();
    const domainsIds = domains.map(domain => domain.id);
    const extendedDomainsIds = new ExtendedArray(...domainsIds);
    const filter = {created: {$gte: startTS, $lt: endTS}};

    extendedDomainsIds.asyncForEach(async (id, i) => {
      console.log(i + 1, '/', domainsIds.length);
      const s = Date.now();

      await Promise.all([
        Banner.find(filter).count(),
        BannerAdded.find(filter).count(),
        BannerLoaded.find(filter).count(),
        BannerShown.find(filter).count(),
        BannerClicked.find(filter).count(),
        BannerlessSection.find(filter).count(),
        ScriptLoaded.find(filter).count(),
        ScriptDownloaded.find(filter).count(),
      ])
      .then(async (couters: number[]) => {
        const [
          banner,
          banner_added,
          banner_loaded,
          banner_shown,
          banner_clicked,
          bannerless_section,
          script_loaded,
          script_downloaded,
        ] = couters;
        
        console.log('banner:', banner);
        console.log('banner_added:', banner_added);
        console.log('banner_loaded:', banner_loaded);
        console.log('banner_shown:', banner_shown);
        console.log('banner_clicked:', banner_clicked);
        console.log('bannerless_section:', bannerless_section);
        console.log('script_loaded:', script_loaded);
        console.log('script_downloaded:', script_downloaded);

        const nodeDomainHour = new NodeDomainHour({
          banner,
          banner_added,
          banner_loaded, 
          banner_shown,
          banner_clicked,
          bannerless_section,
          script_loaded,
          script_downloaded,
          domain: id,
          start: startTS,
          end: endTS,
        });
        await nodeDomainHour.save();
        console.log(`${(Date.now() - s) / 1000}s`);
        console.log('----------');
      });
    });
  }
}