import * as fs from 'fs';


export class Script1505 {

  constructor() {
    this.init();
  }

  init() {
    const file = fs.readFileSync('result1.json', 'utf-8');
    const data: {
      [date: string]: {
        [ip: string]: number
      }
    } = JSON.parse(file);

    const result: {[ip: string]: number} = {};
    Object.values(data).forEach(date => {
      Object.entries(date).forEach(([ip, counter]) => {
        if (!result[ip]) result[ip] = 0;

        result[ip] += counter;
      });
    });

    const list = Object.entries(result)
      .map(([ip, counter]) => {
        return [ip, counter];
      })
      .sort((a, b) => {
        const ac = a[1] as number;
        const bc = b[1] as number;

        if (ac > bc) return -1;
        if (ac < bc) return 1;
        return 0;
      });

    const csv = list.map(x => x.join(',')).join('\n');
    fs.writeFileSync('result.csv', csv);
  }
}